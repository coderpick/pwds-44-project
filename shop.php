<?php
include 'layout/head.php';
?>

<body>

    <!-- Page Wrapper -->
    <div id="wrap" class="layout-1">

        <!-- Top bar -->

        <?php
        include 'layout/top_nav.php';
        ?>
        <!-- Header -->
        <?php
        include 'layout/header.php';
        ?>
        <!--    linking-->
        <div class="linking">
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="GridProducts_4Columns.html#">Home</a></li>
                    <li class="active">Cell Phones &amp; Accessories</li>
                </ol>
            </div>
        </div>
        <div id="content">

            <!-- Products -->
            <section class="padding-top-40 padding-bottom-60">
                <div class="container">
                    <div class="row">

                        <!-- Shop Side Bar -->
                        <div class="col-md-3">
                            <?php include "shop_sidebar.php"; ?>
                        </div>

                        <!-- Products -->
                        <div class="col-md-9">

                            <!-- Short List -->
                            <div class="">
                                <h4>All Products</h4>
                            </div>

                            <!-- Items -->
                            <div class="product-list " style="overflow: hidden;">
                                <div class="item-col-3">
                                    <?php

                                    if (isset($_GET['category']) && isset($_GET['subcategory'])) {

                                        $categorySlug     = $_GET['category'];
                                        $subCategorySlug  = $_GET['subcategory'];

                                        $products = $product->categoryAndSubCategoryProduct($categorySlug, $subCategorySlug);
                                    } elseif (isset($_GET['category'])) {

                                        $categorySlug   = $_GET['category'];
                                        $products = $product->categoryProduct($categorySlug);
                                    } elseif (isset($_GET['brand'])) {

                                        $brandSlug   = $_GET['brand'];
                                        $products = $product->brandProduct($brandSlug);
                                    } else {

                                        $products = $product->getAllProduct();
                                    }

                                    if ($products != null) {
                                        foreach ($products as $product) { ?>
                                            <!-- Product -->
                                            <div class="product">
                                                <article> <img class="img-responsive" src="admin/<?php echo $product->feature_image; ?>" alt="">
                                                    <?php
                                                    if ($product->discount) {
                                                        echo '<span class="sale-tag">-' . $product->discount . '%</span>';
                                                    }
                                                    ?>
                                                    <!-- Content -->
                                                    <span class="tag">
                                                        <?php echo $product->categoryName; ?>
                                                    </span>
                                                    <a href="product.php?slug=<?php echo $product->slug; ?>" class="tittle">
                                                        <?php echo $product->product_name; ?>
                                                    </a>
                                                    <?php
                                                    if ($product->discount) {
                                                        echo '<div class="price">$' . $product->discount_price . ' 
                                                      <span>$' . $product->price . '</span>
                                                      </div>';
                                                    } else {
                                                        echo '<div class="price">$' . $product->price . '</div>';
                                                    }
                                                    ?>

                                                    <a href="" class="cart-btn"><i class="icon-basket-loaded"></i>
                                                    </a>
                                                </article>
                                            </div>

                                    <?php      }
                                    } else {
                                        echo "No products found!";
                                    }
                                    ?>
                                </div>
                            </div>

                            <!-- pagination -->
                            <ul class="pagination">
                                <li> <a href="GridProducts_4Columns.html#" aria-label="Previous"> <i class="fa fa-angle-left"></i> </a> </li>
                                <li><a class="active" href="GridProducts_4Columns.html#">1</a></li>
                                <li><a href="GridProducts_4Columns.html#">2</a></li>
                                <li><a href="GridProducts_4Columns.html#">3</a></li>
                                <li> <a href="GridProducts_4Columns.html#" aria-label="Next"> <i class="fa fa-angle-right"></i> </a> </li>
                            </ul>

                        </div><!-- //.col-md-9 end -->

                    </div>
                </div>
            </section>

            <!-- Your Recently Viewed Items -->
            <section class="padding-bottom-60">
                <div class="container">

                    <!-- heading -->
                    <div class="heading">
                        <h2>Your Recently Viewed Items</h2>
                        <hr>
                    </div>
                    <!-- Items Slider -->
                    <div class="item-slide-5 with-nav owl-carousel owl-theme owl-loaded">
                        <!-- Product -->
                        <div class="owl-stage-outer">
                            <div class="owl-stage" style="transform: translate3d(-2340px, 0px, 0px); transition: all 0.25s ease 0s; width: 4212px;">
                                <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-4.jpg" alt=""> <span class="new-tag">New</span>

                                            <!-- Content -->
                                            <span class="tag">Accessories</span> <a href="GridProducts_4Columns.html#." class="tittle">Teclado Inalambrico Bluetooth Con Air Mouse</a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00</div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-5.jpg" alt="">
                                            <!-- Content -->
                                            <span class="tag">Appliances</span> <a href="GridProducts_4Columns.html#." class="tittle">Funda Para Ebook 7" 128GB full HD</a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00</div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-6.jpg" alt=""> <span class="sale-tag">-25%</span>

                                            <!-- Content -->
                                            <span class="tag">Tablets</span> <a href="GridProducts_4Columns.html#." class="tittle">Mp3 Sumergible Deportivo Slim Con 8GB</a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00 <span>$200.00</span></div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-7.jpg" alt="">
                                            <!-- Content -->
                                            <span class="tag">Appliances</span> <a href="GridProducts_4Columns.html#." class="tittle">Reloj Inteligente Smart Watch M26 Touch Bluetooh </a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00</div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-8.jpg" alt=""> <span class="new-tag">New</span>

                                            <!-- Content -->
                                            <span class="tag">Accessories</span> <a href="GridProducts_4Columns.html#." class="tittle">Teclado Inalambrico Bluetooth Con Air Mouse</a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00</div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-1.jpg" alt="">
                                            <!-- Content -->
                                            <span class="tag">Latop</span> <a href="GridProducts_4Columns.html#." class="tittle">Laptop Alienware 15 i7 Perfect For Playing Game</a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00 </div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-2.jpg" alt=""> <span class="sale-tag">-25%</span>

                                            <!-- Content -->
                                            <span class="tag">Tablets</span> <a href="GridProducts_4Columns.html#." class="tittle">Mp3 Sumergible Deportivo Slim Con 8GB</a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00 <span>$200.00</span></div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-3.jpg" alt="">
                                            <!-- Content -->
                                            <span class="tag">Appliances</span> <a href="GridProducts_4Columns.html#." class="tittle">Reloj Inteligente Smart Watch M26 Touch Bluetooh </a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00</div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-4.jpg" alt=""> <span class="new-tag">New</span>

                                            <!-- Content -->
                                            <span class="tag">Accessories</span> <a href="GridProducts_4Columns.html#." class="tittle">Teclado Inalambrico Bluetooth Con Air Mouse</a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00</div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-5.jpg" alt="">
                                            <!-- Content -->
                                            <span class="tag">Appliances</span> <a href="GridProducts_4Columns.html#." class="tittle">Funda Para Ebook 7" 128GB full HD</a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00</div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item active" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-6.jpg" alt=""> <span class="sale-tag">-25%</span>

                                            <!-- Content -->
                                            <span class="tag">Tablets</span> <a href="GridProducts_4Columns.html#." class="tittle">Mp3 Sumergible Deportivo Slim Con 8GB</a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00 <span>$200.00</span></div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item active" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-7.jpg" alt="">
                                            <!-- Content -->
                                            <span class="tag">Appliances</span> <a href="GridProducts_4Columns.html#." class="tittle">Reloj Inteligente Smart Watch M26 Touch Bluetooh </a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00</div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item active" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-8.jpg" alt=""> <span class="new-tag">New</span>

                                            <!-- Content -->
                                            <span class="tag">Accessories</span> <a href="GridProducts_4Columns.html#." class="tittle">Teclado Inalambrico Bluetooth Con Air Mouse</a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00</div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item cloned active" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-1.jpg" alt="">
                                            <!-- Content -->
                                            <span class="tag">Latop</span> <a href="GridProducts_4Columns.html#." class="tittle">Laptop Alienware 15 i7 Perfect For Playing Game</a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00 </div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item cloned active" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-2.jpg" alt=""> <span class="sale-tag">-25%</span>

                                            <!-- Content -->
                                            <span class="tag">Tablets</span> <a href="GridProducts_4Columns.html#." class="tittle">Mp3 Sumergible Deportivo Slim Con 8GB</a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00 <span>$200.00</span></div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-3.jpg" alt="">
                                            <!-- Content -->
                                            <span class="tag">Appliances</span> <a href="GridProducts_4Columns.html#." class="tittle">Reloj Inteligente Smart Watch M26 Touch Bluetooh </a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00</div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-4.jpg" alt=""> <span class="new-tag">New</span>

                                            <!-- Content -->
                                            <span class="tag">Accessories</span> <a href="GridProducts_4Columns.html#." class="tittle">Teclado Inalambrico Bluetooth Con Air Mouse</a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00</div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                                <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                    <div class="product">
                                        <article> <img class="img-responsive" src="images/item-img-1-5.jpg" alt="">
                                            <!-- Content -->
                                            <span class="tag">Appliances</span> <a href="GridProducts_4Columns.html#." class="tittle">Funda Para Ebook 7" 128GB full HD</a>
                                            <!-- Reviews -->
                                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                            <div class="price">$350.00</div>
                                            <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-controls">
                            <div class="owl-nav">
                                <div class="owl-prev" style=""><i class="fa fa-angle-left"></i></div>
                                <div class="owl-next" style=""><i class="fa fa-angle-right"></i></div>
                            </div>
                            <div class="owl-dots" style="">
                                <div class="owl-dot"><span></span></div>
                                <div class="owl-dot active"><span></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Clients img -->
            <section class="light-gry-bg clients-img">
                <div class="container">
                    <ul>
                        <li><img src="images/c-img-1.png" alt=""></li>
                        <li><img src="images/c-img-2.png" alt=""></li>
                        <li><img src="images/c-img-3.png" alt=""></li>
                        <li><img src="images/c-img-4.png" alt=""></li>
                        <li><img src="images/c-img-5.png" alt=""></li>
                    </ul>
                </div>
            </section>


        </div>

        <!-- Footer -->
        <?php
        include "layout/footer.php";
        ?>

        <!-- GO TO TOP  -->
        <a href="index.php#" class="cd-top"><i class="fa fa-angle-up"></i></a>
        <!-- GO TO TOP End -->
    </div>
    <!-- End Page Wrapper -->

    <!-- JavaScripts -->
    <?php
    include "layout/_script.php";
    ?>

    <!--page require js load here....-->


    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="rs-plugin/js/jquery.tp.t.min.js"></script>
    <script type="text/javascript" src="rs-plugin/js/jquery.tp.min.js"></script>
    <script src="js/main.js"></script>

    <!--custom js activation here....-->


</body>

</html>