<?php
include 'layout/head.php';
?>

<body>

  <!-- Page Wrapper -->
  <div id="wrap" class="layout-1">

    <!-- Top bar -->

    <?php
    include 'layout/top_nav.php';
    ?>
    <!-- Header -->
    <?php
    include 'layout/header.php';
    ?>

    <!-- Linking -->
    <div class="linking">
      <div class="container">
        <ol class="breadcrumb">
          <li><a href="Product-Details.html#">Home</a></li>
          <li><a href="Product-Details.html#">Cell Phones & Accessories</a></li>
          <li class="active">Power Smartphone 7s Silver 128GB/ Camera 7.5”</li>
        </ol>
      </div>
    </div>

    <!-- Content -->
    <div id="content">

      <!-- Products -->
      <section class="padding-top-40 padding-bottom-60">
        <div class="container">
          <div class="row">

            <!-- Shop Side Bar -->
            <div class="col-md-3">
              <?php include "shop_sidebar.php"; ?>
            </div>


            <!-- Products -->
            <div class="col-md-9">

              <?php
              if (isset($_GET['slug'])) {
                $slug = $_GET['slug'];
                $row = $product->productDetail($slug);
              }
              ?>


              <div class="product-detail">
                <div class="product">
                  <div class="row">
                    <!-- Slider Thumb -->
                    <div class="col-xs-5">
                      <article class="slider-item on-nav">
                        <div class="thumb-slider">
                          <ul class="slides">
                            <?php
                            $productImages = $product->getProductImage($row->id);
                            if ($productImages != null) {
                              foreach ($productImages as $productImage) { ?>
                                <li data-thumb="admin/uploads/products/<?php echo $productImage->product_image; ?>"> <img src="admin/uploads/products/<?php echo $productImage->product_image; ?>" alt=""> </li>
                            <?php     }
                            }

                            ?>
                          </ul>
                        </div>
                      </article>
                    </div>
                    <!-- Item Content -->
                    <div class="col-xs-7"> <span class="tags"><?php echo $row->categoryName; ?></span>
                      <h5><?php echo $row->product_name; ?></h5>
                      <div class="row">
                        <div class="col-sm-6"><span class="price">$<?php echo $row->price; ?> </span></div>
                        <div class="col-sm-6">
                          <p>Availability:

                            <?php
                            if ($row->quantity != 0) {
                              echo '  <span class="in-stock">In Stock</span';
                            } else {
                              echo '  <span class="in-stock">Out of Stock</span';
                            }
                            ?>
                          </p>
                        </div>
                      </div>
                      <!-- List Details short description or Feature list -->

                      <p>
                        <?php echo html_entity_decode($row->short_description); ?>
                      </p>

                      <!-- Compare Wishlist -->
                      <ul class="cmp-list">
                        <li><a href="Product-Details.html#."><i class="fa fa-heart"></i> Add to Wishlist</a></li>

                      </ul>
                      <!-- Quinty -->
                      <form action="cart.php" method="post">
                        <div class="quinty">
                          <input type="number" name="qty" min="1" value="1">
                        </div>
                        <input type="hidden" name="productId" value="<?php echo $row->id; ?>">
                        <button type="submit" name="addCart" style="border: none;" class="btn-round"><i class="icon-basket-loaded margin-right-5"></i> Add to Cart</button>

                      </form>
                    </div>
                  </div>

                  <!-- Details Tab Section-->
                  <div class="item-tabs-sec">

                    <!-- Nav tabs -->
                    <ul class="nav" role="tablist">
                      <li role="presentation" class="active"><a href="Product-Details.html#pro-detil" role="tab" data-toggle="tab">Product Details</a></li>
                      <li role="presentation"><a href="Product-Details.html#cus-rev" role="tab" data-toggle="tab">Customer Reviews</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="pro-detil">
                        <?php
                        echo html_entity_decode($row->description);
                        ?>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="cus-rev"></div>

                    </div>
                  </div>
                </div>




                <!-- Related Products -->
                <section class="padding-top-30 padding-bottom-0">
                  <!-- heading -->
                  <div class="heading">
                    <h2>Related Products</h2>
                    <hr>
                  </div>
                  <!-- Items Slider -->
                  <div class="item-slide-4 with-nav">
                    <!-- Product -->
                    <div class="product">
                      <article> <img class="img-responsive" src="images/item-img-1-1.jpg" alt="">
                        <!-- Content -->
                        <span class="tag">Latop</span> <a href="Product-Details.html#." class="tittle">Laptop Alienware 15 i7 Perfect For Playing Game</a>
                        <!-- Reviews -->
                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                        <div class="price">$350.00 </div>
                        <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                      </article>
                    </div>
                    <!-- Product -->
                    <div class="product">
                      <article> <img class="img-responsive" src="images/item-img-1-2.jpg" alt=""> <span class="sale-tag">-25%</span>

                        <!-- Content -->
                        <span class="tag">Tablets</span> <a href="Product-Details.html#." class="tittle">Mp3 Sumergible Deportivo Slim Con 8GB</a>
                        <!-- Reviews -->
                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                        <div class="price">$350.00 <span>$200.00</span></div>
                        <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                      </article>
                    </div>

                    <!-- Product -->
                    <div class="product">
                      <article> <img class="img-responsive" src="images/item-img-1-3.jpg" alt="">
                        <!-- Content -->
                        <span class="tag">Appliances</span> <a href="Product-Details.html#." class="tittle">Reloj Inteligente Smart Watch M26 Touch </a>
                        <!-- Reviews -->
                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                        <div class="price">$350.00</div>
                        <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                      </article>
                    </div>

                    <!-- Product -->
                    <div class="product">
                      <article> <img class="img-responsive" src="images/item-img-1-4.jpg" alt=""> <span class="new-tag">New</span>

                        <!-- Content -->
                        <span class="tag">Accessories</span> <a href="Product-Details.html#." class="tittle">Teclado Inalambrico Bluetooth Con Air Mouse</a>
                        <!-- Reviews -->
                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                        <div class="price">$350.00</div>
                        <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                      </article>
                    </div>

                    <!-- Product -->
                    <div class="product">
                      <article> <img class="img-responsive" src="images/item-img-1-5.jpg" alt="">
                        <!-- Content -->
                        <span class="tag">Appliances</span> <a href="Product-Details.html#." class="tittle">Funda Para Ebook 7" 128GB full HD</a>
                        <!-- Reviews -->
                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                        <div class="price">$350.00</div>
                        <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                      </article>
                    </div>

                    <!-- Product -->
                    <div class="product">
                      <article> <img class="img-responsive" src="images/item-img-1-6.jpg" alt=""> <span class="sale-tag">-25%</span>

                        <!-- Content -->
                        <span class="tag">Tablets</span> <a href="Product-Details.html#." class="tittle">Mp3 Sumergible Deportivo Slim Con 8GB</a>
                        <!-- Reviews -->
                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                        <div class="price">$350.00 <span>$200.00</span></div>
                        <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                      </article>
                    </div>

                    <!-- Product -->
                    <div class="product">
                      <article> <img class="img-responsive" src="images/item-img-1-7.jpg" alt="">
                        <!-- Content -->
                        <span class="tag">Appliances</span> <a href="Product-Details.html#." class="tittle">Reloj Inteligente Smart Watch M26 Touch </a>
                        <!-- Reviews -->
                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                        <div class="price">$350.00</div>
                        <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                      </article>
                    </div>

                    <!-- Product -->
                    <div class="product">
                      <article> <img class="img-responsive" src="images/item-img-1-8.jpg" alt=""> <span class="new-tag">New</span>

                        <!-- Content -->
                        <span class="tag">Accessories</span> <a href="Product-Details.html#." class="tittle">Teclado Inalambrico Bluetooth Con Air Mouse</a>
                        <!-- Reviews -->
                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                        <div class="price">$350.00</div>
                        <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                      </article>
                    </div>
                  </div>
                </section>
              </div>


            </div>
          </div>
      </section>

      <!-- Your Recently Viewed Items -->
      <section class="padding-bottom-60">
        <div class="container">

          <!-- heading -->
          <div class="heading">
            <h2>Your Recently Viewed Items</h2>
            <hr>
          </div>
          <!-- Items Slider -->
          <div class="item-slide-5 with-nav">
            <!-- Product -->
            <div class="product">
              <article> <img class="img-responsive" src="images/item-img-1-1.jpg" alt="">
                <!-- Content -->
                <span class="tag">Latop</span> <a href="Product-Details.html#." class="tittle">Laptop Alienware 15 i7 Perfect For Playing Game</a>
                <!-- Reviews -->
                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                <div class="price">$350.00 </div>
                <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
              </article>
            </div>
            <!-- Product -->
            <div class="product">
              <article> <img class="img-responsive" src="images/item-img-1-2.jpg" alt=""> <span class="sale-tag">-25%</span>

                <!-- Content -->
                <span class="tag">Tablets</span> <a href="Product-Details.html#." class="tittle">Mp3 Sumergible Deportivo Slim Con 8GB</a>
                <!-- Reviews -->
                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                <div class="price">$350.00 <span>$200.00</span></div>
                <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
              </article>
            </div>

            <!-- Product -->
            <div class="product">
              <article> <img class="img-responsive" src="images/item-img-1-3.jpg" alt="">
                <!-- Content -->
                <span class="tag">Appliances</span> <a href="Product-Details.html#." class="tittle">Reloj Inteligente Smart Watch M26 Touch Bluetooh </a>
                <!-- Reviews -->
                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                <div class="price">$350.00</div>
                <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
              </article>
            </div>

            <!-- Product -->
            <div class="product">
              <article> <img class="img-responsive" src="images/item-img-1-4.jpg" alt=""> <span class="new-tag">New</span>

                <!-- Content -->
                <span class="tag">Accessories</span> <a href="Product-Details.html#." class="tittle">Teclado Inalambrico Bluetooth Con Air Mouse</a>
                <!-- Reviews -->
                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                <div class="price">$350.00</div>
                <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
              </article>
            </div>

            <!-- Product -->
            <div class="product">
              <article> <img class="img-responsive" src="images/item-img-1-5.jpg" alt="">
                <!-- Content -->
                <span class="tag">Appliances</span> <a href="Product-Details.html#." class="tittle">Funda Para Ebook 7" 128GB full HD</a>
                <!-- Reviews -->
                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                <div class="price">$350.00</div>
                <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
              </article>
            </div>

            <!-- Product -->
            <div class="product">
              <article> <img class="img-responsive" src="images/item-img-1-6.jpg" alt=""> <span class="sale-tag">-25%</span>

                <!-- Content -->
                <span class="tag">Tablets</span> <a href="Product-Details.html#." class="tittle">Mp3 Sumergible Deportivo Slim Con 8GB</a>
                <!-- Reviews -->
                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                <div class="price">$350.00 <span>$200.00</span></div>
                <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
              </article>
            </div>

            <!-- Product -->
            <div class="product">
              <article> <img class="img-responsive" src="images/item-img-1-7.jpg" alt="">
                <!-- Content -->
                <span class="tag">Appliances</span> <a href="Product-Details.html#." class="tittle">Reloj Inteligente Smart Watch M26 Touch Bluetooh </a>
                <!-- Reviews -->
                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                <div class="price">$350.00</div>
                <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
              </article>
            </div>

            <!-- Product -->
            <div class="product">
              <article> <img class="img-responsive" src="images/item-img-1-8.jpg" alt=""> <span class="new-tag">New</span>

                <!-- Content -->
                <span class="tag">Accessories</span> <a href="Product-Details.html#." class="tittle">Teclado Inalambrico Bluetooth Con Air Mouse</a>
                <!-- Reviews -->
                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                <div class="price">$350.00</div>
                <a href="Product-Details.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
              </article>
            </div>
          </div>
        </div>
      </section>

      <!-- Clients img -->
      <section class="light-gry-bg clients-img">
        <div class="container">
          <ul>
            <li><img src="images/c-img-1.png" alt=""></li>
            <li><img src="images/c-img-2.png" alt=""></li>
            <li><img src="images/c-img-3.png" alt=""></li>
            <li><img src="images/c-img-4.png" alt=""></li>
            <li><img src="images/c-img-5.png" alt=""></li>
          </ul>
        </div>
      </section>


    </div>
    <!-- End Content -->

    <!-- Footer -->
    <!-- Footer -->
    <?php
    include "layout/footer.php";
    ?>
    <!-- Rights -->
    <div class="rights">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <p>Copyright © 2017 <a href="Product-Details.html#." class="ri-li"> SmartTech </a>HTML5 template. All rights reserved</p>
          </div>
          <div class="col-sm-6 text-right"> <img src="images/card-icon.png" alt=""> </div>
        </div>
      </div>
    </div>

    <!-- End Footer -->

    <!-- GO TO TOP  -->
    <a href="Product-Details.html#" class="cd-top"><i class="fa fa-angle-up"></i></a>
    <!-- GO TO TOP End -->
  </div>
  <!-- End Page Wrapper -->
  <!-- JavaScripts -->
  <?php
  include "layout/_script.php";
  ?>

  <!--page require js load here....-->


  <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
  <script type="text/javascript" src="rs-plugin/js/jquery.tp.t.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.tp.min.js"></script>
  <script src="js/main.js"></script>

  <!--custom js activation here....-->
</body>

</html>