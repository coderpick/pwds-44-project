<?php
class ProductController
{

    private  $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getAllProduct()
    {
        $sql = "SELECT product.id,product.product_name,product.slug,product.price,product.discount,product.discount_price,product.feature_image,category.name as categoryName FROM product
        INNER JOIN category ON product.category_id = category.id WHERE product.status=true
        ORDER BY product.id";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function productDetail($slug)
    {
        $sql = "SELECT product. * ,category.name as categoryName FROM product
        INNER JOIN category ON product.category_id = category.id WHERE  product.slug =:slug";
        $this->db->query($sql);
        $this->db->bind(":slug", $slug);
        $this->db->execute();
        return $this->db->single();
    }

    public function getProductImage($id)
    {
        $sql = "SELECT * FROM product_images WHERE product_id=:productId";
        $this->db->query($sql);
        $this->db->bind(":productId", $id);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function categoryProduct($slug)
    {
        $sql = "SELECT * FROM category WHERE slug=:categorySlug";
        $this->db->query($sql);
        $this->db->bind(":categorySlug", $slug);
        $this->db->execute();
        $category = $this->db->single();
        if (!empty($category)) {
            $sql = "SELECT product.id,product.product_name,product.slug,product.price,product.discount,product.discount_price,product.feature_image,category.name as categoryName FROM product INNER JOIN category ON product.category_id = category.id WHERE product.status=true AND product.category_id=:categoryId ORDER BY product.id";
            $this->db->query($sql);
            $this->db->bind(":categoryId", $category->id);
            $this->db->execute();
            return $this->db->resultSet();
        }
    }

    public function brandProduct($slug)
    {
        $sql = "SELECT * FROM brand WHERE slug=:brandSlug";
        $this->db->query($sql);
        $this->db->bind(":brandSlug", $slug);
        $this->db->execute();
        $brand = $this->db->single();
        if (!empty($brand)) {
            $sql = "SELECT product.id,product.product_name,product.slug,product.price,product.discount,product.discount_price,product.feature_image,category.name as categoryName FROM product INNER JOIN category ON product.category_id = category.id WHERE product.status=true AND product.brand_id=:brandId ORDER BY product.id";
            $this->db->query($sql);
            $this->db->bind(":brandId", $brand->id);
            $this->db->execute();
            return $this->db->resultSet();
        }
    }



    public function categoryAndSubCategoryProduct($categorySlug, $subCategorySlug)
    {

        // get category id by requested slug
        $sql = "SELECT * FROM category WHERE slug=:categorySlug";
        $this->db->query($sql);
        $this->db->bind(":categorySlug", $categorySlug);
        $this->db->execute();
        $category = $this->db->single();

        // get sub category id by requested slug
        $sql = "SELECT * FROM sub_category WHERE slug=:subCategorySlug";
        $this->db->query($sql);
        $this->db->bind(":subCategorySlug", $subCategorySlug);
        $this->db->execute();
        $subCategory = $this->db->single();

        if (!empty($category) && !empty($subCategory)) {

            $sql = "SELECT product.id,product.product_name,product.slug,product.price,product.discount,product.discount_price,product.feature_image,category.name as categoryName FROM product INNER JOIN category ON product.category_id = category.id WHERE product.status=true AND product.category_id=:categoryId AND product.sub_category_id=:subCatId ORDER BY product.id DESC";
            $this->db->query($sql);
            $this->db->bind(":categoryId", $category->id);
            $this->db->bind(":subCatId", $subCategory->id);
            $this->db->execute();
            return $this->db->resultSet();
        }
    }
}
