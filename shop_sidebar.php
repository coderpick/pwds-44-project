<div class="shop-side-bar">

    <!-- Featured Brands -->
    <h6>Featured Brands</h6>
    <div class="checkbox checkbox-primary">
        <ul>
            <?php
            $brands = $home->brand();
            if (!empty($brands)) {
                foreach ($brands as $brand) { ?>

                    <li>
                        <a href="shop.php?brand=<?php echo $brand->slug; ?>"><?php echo $brand->name; ?></a>
                    </li>
            <?php         }
            }
            ?>
        </ul>
    </div>
    <!-- Categories -->
    <h6>Categories</h6>
    <div class="checkbox checkbox-primary">
        <ul>
        <?php
          $categories = $home->getCategory();
            if (!empty($categories)) {
                foreach ($categories as $category) {?>

                    <li>
                        <a href="shop.php?category=<?php echo $category->slug; ?>"><?php echo $category->name; ?></a>
                    </li>
            <?php         }
            }
            ?>

        </ul>
    </div>

    <!-- Categories -->

</div>